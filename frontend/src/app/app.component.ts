import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ServiciosEndpointService } from './servicios/servicios-endpoint.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'frontend'
  formOrden:FormGroup
  submitted:boolean = false
  respuesta:object|any
  inputResultado:boolean = false
  inputValue:string|any
  valor:string|any = ''
  grupo:object|any = []
  res:[]|any
  cargo:[]|any =[]
  errorFormulario:boolean = false
  msg:string|any = ""
  private ejecutar:boolean = false
  private URL = 'http://172.17.0.3:3001/api/v1/services/listado/cargoes'
  private parametros:Array<any> = []


  constructor(private formBuilder: FormBuilder,
  	private serviciosEndpointService: ServiciosEndpointService){
  	//[Validators.required, Validators.minLength(10)]
  	this.formOrden = this.formBuilder.group({
      grupo: ['', Validators.required],
      cargo : ['', Validators.required],
      nombre : ['', [Validators.required, Validators.maxLength(50)]]
    });
  }

  async ngOnInit() {
    //obtenergrupos
    await this.obtenerGrupo()
    await this.obtenerCargo()
  }

  obtenerGrupo() {
  	this.serviciosEndpointService.getData('http://172.17.0.3:3001/api/v1/services/listar/grupo/')
  	.toPromise().then(data => {
        this.res = data
        this.grupo = this.res.rows//Object.keys(this.res)
      })
  }

  obtenerCargo(){
    this.serviciosEndpointService.getData('http://172.17.0.3:3001/api/v1/services/listar/cargo/')
    .toPromise().then(data => {
        this.res = data
        this.cargo = this.res.rows//Object.keys(this.res)
      })
  	// let arrcargo:[]|any = []
  	// for(const [key, value] of Object.entries(this.res)){
  	// 	if(key == e.target.value) {
  	// 		this.cargo = value
  	// 	}
  	// }
  }

  get f() {
    return this.formOrden.controls;
  }

  guardarDatos(event) {
  	console.log(this.formOrden.value)
    if (this.formOrden.invalid) {
      this.submitted = true;
      return;
    } else {
    	this.serviciosEndpointService.saveData('http://172.17.0.3:3001/api/v1/services/insertar/datos/',this.formOrden.value)
    	.toPromise().then(data => {
    		let res:object|any = data
    		if(res.code == 200){
    			this.formOrden.reset()
    		}else {
    			this.errorFormulario = true
          this.msg = res.msg
    		}
      })
    }
  }

}
