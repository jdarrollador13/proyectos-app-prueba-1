-- SEQUENCE: public.Employee_Id_seq

-- DROP SEQUENCE public."Employee_Id_seq";

CREATE SEQUENCE public."Employee_Id_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
ALTER SEQUENCE public."Employee_Id_seq"
    OWNER TO postgres;

-- Table: public.Employee
-- DROP TABLE public."Employee";
CREATE TABLE public."Employee"
(
    "Id" integer NOT NULL DEFAULT nextval('"Employee_Id_seq"'::regclass),
    "Nombre" text COLLATE pg_catalog."default",
    "IdRankEmployee" integer,
    "IdGroupEmployee" integer,
    CONSTRAINT "Employee_pkey" PRIMARY KEY ("Id")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public."Employee"
    OWNER to postgres;

--***********************************
--***********************************
-- SEQUENCE: public.GroupEmployee_Id_seq
-- DROP SEQUENCE public."GroupEmployee_Id_seq";
CREATE SEQUENCE public."GroupEmployee_Id_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
ALTER SEQUENCE public."GroupEmployee_Id_seq"
    OWNER TO postgres;

-- Table: public.GroupEmployee
-- DROP TABLE public."GroupEmployee";
CREATE TABLE public."GroupEmployee"
(
    "Id" integer NOT NULL DEFAULT nextval('"GroupEmployee_Id_seq"'::regclass),
    "Nombre" text COLLATE pg_catalog."default",
    "Codigo" text COLLATE pg_catalog."default",
    CONSTRAINT "GroupEmployee_pkey" PRIMARY KEY ("Id")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public."GroupEmployee"
    OWNER to postgres;


--***********************************
--***********************************
-- SEQUENCE: public.RankEmployee_Id_seq
-- DROP SEQUENCE public."RankEmployee_Id_seq";
CREATE SEQUENCE public."RankEmployee_Id_seq"
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
ALTER SEQUENCE public."RankEmployee_Id_seq"
    OWNER TO postgres;

-- Table: public.RankEmployee
-- DROP TABLE public."RankEmployee";
CREATE TABLE public."RankEmployee"
(
    "Id" integer NOT NULL DEFAULT nextval('"RankEmployee_Id_seq"'::regclass),
    "Nombre" text COLLATE pg_catalog."default",
    "Codigo" text COLLATE pg_catalog."default",
    CONSTRAINT "RankEmployee_pkey" PRIMARY KEY ("Id")
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;
ALTER TABLE public."RankEmployee"
    OWNER to postgres;