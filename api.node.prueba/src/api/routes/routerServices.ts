import { Request, Response, NextFunction, Router } from 'express'
import ServicesController from '../../controllers/servicesController'
import { Container } from "typescript-ioc";


export default class routerServices {
  public app: Router
  constructor(router: Router) {
    this.app = router
  }
  router(): void {
  this.app.get(
      '/services/',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          console.log('app1')
          let responseModel = {status:200, msg:'ok'}
          res.status(200).json(responseModel);
        } catch (error) {
          console.log(error)
        }
      }
    )

  this.app.get(
      '/services/schema/',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          console.log('app2')
          const servicesController: ServicesController = Container.get(ServicesController);
          let responseModel = await servicesController.listarSchema();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log(error)
        }
      }
    )
  /**
   *@method : get
  * DEVUELVE SERVICO DE CIUDADES
  **/
  /*"exec": "SET NODE_ENV=test&& ts-node ./src/app.ts"*/
    this.app.get(
      '/services/listado/',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          console.log('app2')
          const servicesController: ServicesController = Container.get(ServicesController);
          let responseModel = await servicesController.listar();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log('app2 --error')
          console.log(error)
        }
      }
    )

    this.app.post(
      '/services/insertar/datos/',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
           console.log('app3')
          const servicesController: ServicesController = Container.get(ServicesController);
          let responseModel = await servicesController.guardar(req.body);
          res.status(200).json(responseModel);
        } catch (error) {
          console.log('app2 --error 3')
          console.log(error)
        }
      }
    )

    this.app.get(
      '/services/listar/grupo/',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          console.log('app4')
          const servicesController: ServicesController = Container.get(ServicesController);
          let responseModel = await servicesController.listarGrupo();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log('app4 --error')
          console.log(error)
        }
      }
    )

    this.app.get(
      '/services/listar/cargo/',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          console.log('app5')
          const servicesController: ServicesController = Container.get(ServicesController);
          let responseModel = await servicesController.listarCargo();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log('app5 --error')
          console.log(error)
        }
      }
    )
    this.app.get(
      '/services/bases/',
      async (req: Request, res: Response, next: NextFunction) => {
        try {
          console.log('app5')
          const servicesController: ServicesController = Container.get(ServicesController);
          let responseModel = await servicesController.listarBases();
          res.status(200).json(responseModel);
        } catch (error) {
          console.log('app5 --error')
          console.log(error)
        }
      }
    )
  }
}