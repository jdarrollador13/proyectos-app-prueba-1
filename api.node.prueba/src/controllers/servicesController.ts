import { Request, Response, NextFunction } from 'express'
import { Inject } from "typescript-ioc";
import { ServicesDAO } from '../DAO/ServicesDAO'
import requests from 'request-promise'


export default class ServicesController {
	constructor(
		@Inject private servicesDAO: ServicesDAO,
	) {
	}
 /**
  * DEVUELVE SERVICO DE CIUDADES
	**/
	async listar():Promise<any> {
		let res:any;
		try{
			res = await this.servicesDAO.listar()
			return res
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
		
	}

	async guardar(requets:object|any): Promise<any> {
		let res:any;
		try {
			let {grupo,cargo,nombre}  = requets
			let Nombre = nombre 
			let IdRankEmployee = cargo 
			let IdGroupEmployee = grupo
			if (Nombre == '' && IdRankEmployee == '' && IdGroupEmployee) {
				res = { 'code' :400, 'msg' : 'campos obligatorios'}
				return res
			}
			let vIdGroupEmployee:any = await this.servicesDAO.Validar(IdGroupEmployee,'GroupEmployee')
			if(vIdGroupEmployee.code != 200){
				res = { 'code' :400, 'msg' : 'campos IdGroupEmployee invalido'}
				return res
			}
			let vIdRankEmployee:any = await this.servicesDAO.Validar(IdRankEmployee,'RankEmployee')
			if(vIdRankEmployee.code != 200){
				res = { 'code' :400, 'msg' : 'campos IdRankEmployee invalido'}
				return res
			}
			let vNomEmployee:any = await this.servicesDAO.ValidarEmpleado(Nombre,'Employee')
			if(vIdRankEmployee.code == 200){
				res = { 'code' :400, 'msg' : 'El emplado ya existe.'}
				return res
			}

			res = await this.servicesDAO.guardar(requets)
			return res
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}

	async listarGrupo():Promise<any> {
		let res:any;
		try{
			res = await this.servicesDAO.listarGrupo()
			return res
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}
	

	async listarCargo():Promise<any> {
		let res:any;
		try{
			res = await this.servicesDAO.listarCargo()
			return res
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}

	async listarSchema():Promise<any> {
		let res:any;
		try{
			res = await this.servicesDAO.listarSchema()
			return res
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}

	async listarBases():Promise<any> {
		let res:any;
		try{
			res = await this.servicesDAO.listarBases()
			return res
		}catch(error){
			res = { 'code' :500, 'msg' : 'error'}
			return res
		}
	}

}
