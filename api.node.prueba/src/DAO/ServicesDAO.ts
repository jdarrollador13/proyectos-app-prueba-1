import Conection from '../loaders/databaseLoader'
import { Inject } from "typescript-ioc";

export class ServicesDAO {

	constructor(
		@Inject private databaseConnection: Conection
	) { }

	/**
	@router 
    **/
	public async listar(): Promise<any> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT em."Id", em."Nombre", em."IdRankEmployee",
																								  em."IdGroupEmployee", gr."Nombre" AS GrNombre, gr."Codigo" AS GrCodigo,
																									rk."Nombre" AS RkNombre, rk."Codigo" AS RkCodigo
																								FROM public."Employee" em 
																								INNER JOIN public."GroupEmployee" gr ON em."IdGroupEmployee" = gr."Id"
																								INNER JOIN public."RankEmployee" rk ON em."IdGroupEmployee" = rk."Id";`);
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'rows' : query.rows} 
			}else{
				data = { 'code' :201, 'rows' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}

	/**
	@router 
    **/
	public async guardar(reqruest:object|any): Promise<any> {
		let data: any
		try {
			let {grupo,cargo,nombre}  = reqruest
			let Nombre = nombre 
			let IdRankEmployee = cargo 
			let IdGroupEmployee = grupo
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`INSERT INTO public."Employee"("Nombre", "IdRankEmployee", "IdGroupEmployee") VALUES($1, $2, $3)`, [Nombre,IdRankEmployee,IdGroupEmployee]);
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'msg' : "Proceso Exitoso"} 
			}else{
				data = { 'code' :201, 'msg' : "Proceso ejecuto con error"} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'Proceso ejecuto error' } 
			return data
		}
	}

	/**
	@router 
    **/
	public async Validar(IdTable:number|any,table:string): Promise<any> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT "Id", "Nombre", "Codigo" FROM public."${table}" WHERE "Id" = $1;`,[IdTable]);
			console.log('queryy',query)
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'msg' : "encontrado"} 
			}else{
				data = { 'code' :201, 'msg' : "error"} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}

	/**
	@router 
    **/
	public async ValidarEmpleado(NomEmpleado:string|any,table:string): Promise<any> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT "Id", "Nombre" FROM public."${table}" WHERE "Nombre" = $1;`,[NomEmpleado]);
			console.log('queryy',query)
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'msg' : "encontrado"} 
			}else{
				data = { 'code' :201, 'msg' : "error"} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}

	public async listarGrupo(): Promise<any> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT "Id", "Nombre", "Codigo" FROM public."GroupEmployee";`);
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'rows' : query.rows} 
			}else{
				data = { 'code' :201, 'rows' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}
	
	public async listarCargo(): Promise<any> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT "Id", "Nombre", "Codigo" FROM public."RankEmployee";`);
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'rows' : query.rows} 
			}else{
				data = { 'code' :201, 'rows' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}

	public async listarSchema(): Promise<any> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT table_schema, table_name
														FROM information_schema.tables
														ORDER BY table_name;`);
			console.log('--- query',query)
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'rows' : query.rows} 
			}else{
				data = { 'code' :201, 'rows' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}

	public async listarBases(): Promise<any> {
		let data: any
		try {
		  const connection = await this.databaseConnection.getPool()
			const query:any = await connection.query(`SELECT datname FROM pg_database;`);
			console.log('--- query',query)
			if(query.rowCount > 0 ){
				data = { 'code' :200, 'rows' : query.rows} 
			}else{
				data = { 'code' :201, 'rows' : []} 
			}
			return data
		}catch(error) {
			console.log(error)
			data ={ 'code' :500,'msg' : 'error' } 
			return data
		}
	}
}