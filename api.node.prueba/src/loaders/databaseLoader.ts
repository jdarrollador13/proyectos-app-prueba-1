//import mysql from 'mysql';
import { Pool } from 'pg'
import { Singleton } from 'typescript-ioc';
import config from '../config';

/**
 * @category Database
 */
@Singleton
export default class DatabaseConnection {
    constructor() {
       
    }

    public async getPool() {
      console.log('*************')
        const connection = new Pool({
          //host: 'localhost',
          //host: 'localhost',
          host: '172.17.0.2',
          user: 'postgres',
          database: 'b_ancolombia',
          password: 'admin',
          //port: 5432,
          max: 20,
          idleTimeoutMillis: 30000,
          connectionTimeoutMillis: 2000,
        })
      return connection 
      console.log('*************')
    }
}